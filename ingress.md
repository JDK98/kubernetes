INGRESS
---
```
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: myapp-ingress
spec:
  rules:
  - host: myapp.com
    http: 
      paths:
      - backend:
          serviceName: myapp-internal-service
          servicePort: 80
```
```
Good One
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: mongo-ingress
  annotations:
    kubernetes.io/ingress.class: nginx
spec:
  rules:
  - host: xxx
    http:
        paths:
        - pathType: Prefix
          path: "/"
          backend:
            service:
              name: helm-mongo-express-service
              port:
                number: 8081
```
**Ingress with Multiple path**
```
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: myapp-ingress
spec:
  rules:
  - host: myapp.com
    http: 
      paths:
      - path: /analytics
        backend:
          serviceName: myapp-internal-service
          servicePort: 80
      - path: /shopping
        backend:
          serviceName: myapp-internal-service
          servicePort: 80
```
**Ingress with multiple subdomain**
```
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: myapp-ingress
spec:
  rules:
  - host: analytics.myapp.com
    http: 
      paths:
        backend:
          serviceName: analytics-svc
          servicePort: 3000
  - host: shopping.myapp.com
    http: 
      paths:
        backend:
          serviceName: shopping-svc
          servicePort: 3000
```

**Configure Ingress with https**
```
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: myapp-ingress
spec:
  tls:
  - hosts: 
    - myapp.com
    secretName: myapp-secret-tls
  rules:
  - host: myapp.com
    http: 
      paths:
      - path: /
        backend:
          serviceName: myapp-internal-service
          servicePort: 8080
```
```
apiVersion: v1
kind: Secret
metadata:
    name: myapp-secret-tls
    namespace: default
date:
    tls.crt: base64 encoded cert
    tls.key: base64 encoded key
type: kubernetes.io/tls
```

