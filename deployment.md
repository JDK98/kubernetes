DEPLOYMENT
---

**Deployment created from CLI** <br>
`kubectl create deployment nginx-deploy --image=nginx`

**Deployment created from file - Generic syntax sample**

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    app: nginx
spec:
  replicas: 1
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.7.9
        ports:
        - containerPort: 80
```
