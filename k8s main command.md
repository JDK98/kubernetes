KUBERNETES MAIN COMMAND
---

- `kubectl get [component]` Lister les composants  <br>
- `kubectl describe [component][name]` Afficher des infos (état etc) sur le composants
- `kubectl logs [name]` Afficher les logs
- `kubectl exec -it [name] -- /bin/bash` Accéder au conteneur du deployment
- `kubectl delete [component][name]` Supprimer un composant
- `kubectl apply -f [file_name]` Exécute un fichier de configuration
- `kubectl create [component][name]` Créer un composant
- `kubectl edit [component][name]` Edite un composant 
