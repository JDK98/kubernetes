Configmap
---

```
apiVersion: v1
kind: ConfigMap
metadata:
  name: mongodb-url
data:
  database_url: mongodb-service
```
