HELM
---

Helm est le package manager de Kubernetes <br>
Le registry public est Helm hub

- Créer un cluster Kubernetes sur Linode <br>

- Télécharger la clé et se connecter au cluster <br>
`export KUBECONFIG=helm-demo-kubeconfig.yaml`

- Ajouter et rechercher la charte mongodb <br>
`helm repo add bitnami https://charts.bitnami.com/bitnami`
`helm search repo bitnami/mongo`

- Créer les mongo statefulset en overridant certaines valeurs avec un .yaml <br>
`helm install mongodb --values helm-mongodb.yaml bitnami/mongodb` 

    *mongodb est un nom aléatoire donné à la charte <br>
    *helm-mongodb.yaml est un yaml contenant des valeurs overridant les paramètres par défaut de la charte, 
    également possible d'écrire --set parameters.properties=value <br>

- Créer le deployment mongo-express pour une administration graphique de mongo <br>
`kubectl apply -f helm-mongo-express.yaml`

- Ajouter le controleur Nginx Ingress qui sera l'interface public à notre application <br>
```
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update
helm install ingress-nginx ingress-nginx/ingress-nginx --set controller.publishService.enabled=true
```

- Créer le component Ingress qui définira les routes de notre application <br>
`kubectl apply -f helm-ingress.yaml`

- Tester le hostname du NodeBalancer dans un navigateur
`nb-185-3-93-4.london.nodebalancer.linode.com`

Pour reduire le nombre de replicas mongodb `kubectl scale --replicas=1 statefulset.apps/mongodb`


