Install Kubernetes with Minikube
---

- **Créer l'instance minimale (t3.small)** <br>

- **Installation du binaire kubectl avec curl** <br>
```
curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl
``` 
<br>

- **Rendre le binaire executable** <br>
`chmod +x ./kubectl` <br>

- **Déplacer le binaire dans le repertoire d'executable**  <br>
`sudo mv ./kubectl /usr/local/bin/kubectl`

- **Tester la version Kubectl installé** <br>
`kubectl version --client`

- **Installation de Docker** <br>
`sudo apt-get update -y &&  sudo apt-get install -y docker.io`

- **Tester la version de Docker installé** <br>
`sudo docker version`

- **Installation de Minikube**  <br>
```
curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 \
&& chmod +x minikube \
&& sudo mv minikube /usr/local/bin/
```

- **Tester la version de minikube installé** <br>
`minikube version ` <br>

- Démarrer minikube  <br>
```
sudo -i
minikube start --driver=none
```

- **Installation de conntrack** <br>
Si erreur **Sorry, Kubernetes v1.18.0 requires conntrack to be installed in root's path** <br>
Executer `apt install conntrack` <br>

- **Tester l'etat de minikube**  <br>
`minikube status`
